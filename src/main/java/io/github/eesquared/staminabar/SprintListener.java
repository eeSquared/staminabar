package io.github.eesquared.staminabar;

import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerToggleSprintEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class SprintListener implements Listener{

	private StaminaBar plugin;
	// TODO: Graphic elements (replace hunger icons, show timers on screen)
	// TODO: Event for level increase; change player's loss and regen speed
	//https://jd.bukkit.org/org/bukkit/entity/Player.html#getExhaustion()
	//http://minecraft.gamepedia.com/Hunger#Mechanics	

	public SprintListener(StaminaBar pl) {
		plugin = pl;
	}

	@EventHandler
	public void onSprintToggle(PlayerToggleSprintEvent e){
		long temp;
		// Get the player who triggered the event
		Player p = e.getPlayer();
		// Retrieve player data
		PlayerData data;
		if(plugin.containsPlayer(p.getName())){
			data = plugin.getPlayerData(p.getName());
		}
		// If no player data is stored, create the default and save to file
		else{
			data = new PlayerData(p.getName(), 500, 1000);
			plugin.players.add(data);
			PlayerDataLoader.saveData(plugin.players, plugin);
		}
		int lossSpeed = data.lossSpeed;
		int regenSpeed = data.regenSpeed;
		
		if(e.isSprinting()){
			p.sendMessage("You're sprinting");
			// Decrease food according to loss speed until player can no longer sprint
			// TODO: break out of this loop if the player manually stops sprinting
			temp = System.currentTimeMillis();
			while(p.getFoodLevel() > 3){
				while(System.currentTimeMillis() - temp < lossSpeed){
					// Wait and do nothing
				}
				temp = System.currentTimeMillis();
				// TODO: The change only becomes visible after the player stops sprinting, and the player jumps back a lot
				p.sendMessage("-1 food");
				p.setFoodLevel(p.getFoodLevel()-1);
			}
		}
		else{
			p.sendMessage("You stopped");
			// When a player stops sprinting (on their own or by running out of stamina), regenerate food according to regen speed
			temp = System.currentTimeMillis();
			while(p.getFoodLevel() < 20){
				while(System.currentTimeMillis() - temp < regenSpeed){
					// Wait and do nothing
				}
				temp = System.currentTimeMillis();
				// TODO: The change only becomes visible when the bar is full again, and the player jumps back a bit
				p.sendMessage("+1 food");
				p.setFoodLevel(p.getFoodLevel()+1);
			}
		}
	}

	@EventHandler
	public void onEat(PlayerItemConsumeEvent e){
		// If a player eats food, give them a temporary buff to both loss and regen speed
		if(e.getItem().getData().getItemType().isEdible()){
			// Get the player who triggered the event
			Player p = e.getPlayer();
			// Retrieve player data
			PlayerData data;
			if(plugin.containsPlayer(p.getName())){
				data = plugin.getPlayerData(p.getName());
			}
			// If no player data is stored, create the default and save to file
			else{
				data = new PlayerData(p.getName(), 500, 1000);
				plugin.players.add(data);
				PlayerDataLoader.saveData(plugin.players, plugin);
			}
			int lossSpeed = data.lossSpeed;
			int regenSpeed = data.regenSpeed;
			plugin.players.remove(data);
			PlayerData newData =  new PlayerData(p.getName(), lossSpeed+100, regenSpeed-100);
			plugin.players.add(newData);
			long temp = System.currentTimeMillis();
			// TODO: Test and see if sprint events will abide by the changes while loop is running
			while(System.currentTimeMillis() - temp < 10000){
				// Wait before returning values to their normal state
			}
			// Reset values
			plugin.players.remove(newData);
			plugin.players.add(data);
		}
	}
}
