package io.github.eesquared.staminabar;


public class PlayerData {
	public String name;
	public int lossSpeed;
	public int regenSpeed;
	
	public PlayerData(String n, int l, int r){
		name = n;
		lossSpeed = l;
		regenSpeed = r;
	}

}
