package io.github.eesquared.staminabar;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PlayerDataLoader {
	
	// Loads all saved player data from the file plugins/Staminaar/staminadata.txt
	public static ArrayList<PlayerData> loadData(StaminaBar plugin) {
		ArrayList<PlayerData> players = new ArrayList<PlayerData>();

		try {
			// Check if plugins/StaminaBar exists; if not, create it
			File dir = new File(plugin.getDataFolder() + "/");
			if(!dir.exists()) {
				dir.mkdir();
			}

			// Check if staminadata.txt exists; if not, return an empty ArrayList
			File file = new File(plugin.getDataFolder() + "/staminadata.txt");
			if(!file.exists()){
				return players;
			}

			// Read the file and populate ArrayList with saved data
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			String l;
			while((l=br.readLine()) != null)
			{
				// Parse file for components of the current ParkourRegion
				String name = l;
				int lossSpeed = Integer.parseInt(br.readLine());
				int regenSpeed = Integer.parseInt(br.readLine());

				// Create new ParkourRegion from data 
				PlayerData p = new PlayerData(name, lossSpeed, regenSpeed);
				
				// Add fully populated PlayerData to ArrayList and continue
				players.add(p);
				br.readLine();
			}
			// Close file
			br.close();
		}
		// If an IO exception is encountered, print the stack trace
		catch(IOException ioe) {
			ioe.printStackTrace();
		}

		// Returns a fully populated ArrayList of PlayerDatas
		return players;
	}


	// Saves all ParkourRegions in plugin to the file plugins/ParkourPlugin/parkours.txt
	public static void saveData(ArrayList<PlayerData> players, StaminaBar plugin) {
		try {
			// Check if the directory plugins/StaminaBar exists; if not, create it
			File dir = new File(plugin.getDataFolder() + "/");
			if(!dir.exists()) {
				dir.mkdir();
			}
			// Check if the file staminadata.txt exists; if so, delete it as it will be overwritten
			File file = new File(plugin.getDataFolder() + "/staminadata.txt");
			if(file.exists()){
				file.delete();
			}

			// For each player, write their data to file
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			for(PlayerData p : players){
				// Write name
				bw.write(p.name);
				bw.newLine();
				// Write stamina loss speed
				bw.write(String.valueOf(p.lossSpeed));
				bw.newLine();
				// Write stamina regen speed
				bw.write(String.valueOf(p.regenSpeed));
				bw.newLine();
			}
			bw.newLine();

			// Close file
			bw.close();
		}
		// If an IO exception is encountered, print the stack trace
		catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}

}
