package io.github.eesquared.staminabar;

import java.util.ArrayList;

import org.bukkit.plugin.java.JavaPlugin;

public class StaminaBar extends JavaPlugin {
	public ArrayList<PlayerData> players = new ArrayList<PlayerData>();
	
    @Override
    public void onEnable() {
    	getLogger().info("StaminaBar enabled");
    	
    	// Set listener
    	getServer().getPluginManager().registerEvents(new SprintListener(this), this);
    	
    	// Load data
    	getLogger().info("Loading data...");
    	players = PlayerDataLoader.loadData(this);
    	getLogger().info("Done");
    }
    
    @Override
    public void onDisable() {
    	// Save data
    	getLogger().info("Saving data...");
    	PlayerDataLoader.saveData(players, this);
    	getLogger().info("Done");
    	
    	getLogger().info("StaminaBar disabled");
    }
    
    public boolean containsPlayer(String name){
    	for(PlayerData p : players){
    		if(p.name.equals(name)){
    			return true;
    		}
    	}
    	return false;
    }
    
    public PlayerData getPlayerData(String name){
    	for(PlayerData p : players){
    		if(p.name.equals(name)){
    			return p;
    		}
    	}
    	return null;
    }
}
